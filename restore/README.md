## DB Tools
Guide how to restore Mongo database using Mongo Database Tools

### Prerequisites
- Check server's free storage
- [Install Mongo Database Tools](https://www.mongodb.com/docs/database-tools/installation/installation/)
- Make a 'dump' folder
- Move your .tgz backup file in to 'dump' folder and unzip
---

### Standalone
Open terminal and run
- Local\
  `mongorestore --useranme <your_db_username> --password <your_db_password> --authenticationDatabase admin --authenticationMecahnism SCRAM-SHA-1`
- Remote using uri\
  `mongorestore --uri mongodb+srv://"<your_db_username>":"<your_db_password>"@<your_db_host> --authenticationDatabase admin --authenticationMecahnism SCRAM-SHA-1`
- Remote using ip as host\
  `mongorestore --useranme <your_db_username> --password <your_db_password> --host <your_db_host> --port <your_db_port> --authenticationDatabase admin --authenticationMecahnism SCRAM-SHA-1`
### Replica Set
Open terminal and run
- Local \
  `mongorestore --oplogReplay --useranme <your_db_username> --password <your_db_password> --authenticationDatabase admin --authenticationMecahnism SCRAM-SHA-1`
- Remote using uri\
  `mongorestore --oplogReplay --uri mongodb+srv://"<your_db_username>":"<your_db_password>"@<your_db_host> --authenticationDatabase admin --authenticationMecahnism SCRAM-SHA-1`
- Remote using ip as host\
  `mongorestore --oplogReplay --useranme <your_db_username> --password <your_db_password> --host <your_db_host> --port <your_db_port> --authenticationDatabase admin --authenticationMecahnism SCRAM-SHA-1`




