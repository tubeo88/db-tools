#!/bin/sh
#=======================================
# Variables
#=======================================
db_name="DB_REMOTE"
db_user="db_user"
db_pass="db_pass"
db_host="db_host"
db_port="db_port"
db_authdb="admin"
days_store="days_to_store"
db_auth_mech="SCRAM-SHA-1"
dump_time=$(date +%F-%H%M)
#=======================================
# passing arguments to command
OPTIND=1
while getopts "u:k:" opt
do
   case "$opt" in
      u ) db_user="$OPTARG" ;;
      k ) db_pass="$OPTARG" ;;
   esac
done
OPTIND=1
#=======================================
echo "start dumping database $db_name"
echo "..............................."
# Dump
mongodump --oplog --readPreference=secondary --forceTableScan --username "$db_user" --password "$db_pass" --authenticationDatabase $db_authdb --authenticationMechanism $db_auth_mech --out=dump/"$dump_time"
# Compress backup folder
tar -zcvf dump/"$dump_time".tgz -C dump/"$dump_time"/ .
# Delete uncompressed folder
rm -rf dump/"$dump_time"
echo "..............................."
echo "dump completed!"

