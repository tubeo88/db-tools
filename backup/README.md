## DB Tools
Bash scripts to backup Mongo database using Mongo Database Tools

### Prerequisites
- Check server's free storage
- [Install Mongo Database Tools](https://www.mongodb.com/docs/database-tools/installation/installation/)
- Common command arguments
  - -u : username
  - -k : password
  - -h : host
  - -p : port
---

### Standalone Local
- ##### Manual
Run in terminal
```
source local-standalone.sh -u '<your_db_username>' -k '<your_db_password>' 
```
Example
```
source local-standalone.sh -u 'root' -k 'root'
```
- ##### Automatic
Automate backup process with crontab\
Run in terminal
```
crontab -e
```
And then add your cronjob that suits you the most.
Example of cronjob runs every day at 0:00 AM
```
0 0 * * * source /path/to/scripts/local-standalone.sh -u '<your_db_username>' -k '<your_db_password>'
```
---
### Standalone Remote
- #### Manual
##### Using MongoBD uri
Run in terminal
```
source remote-standalone-uri.sh -u '<your_db_username>' -k '<your_db_password>' -h '<your_db_host>' 
```
Example
```
source remote-standalone-uri.sh -u 'root' -k 'root' -h 'cluster0.ytimstn.mongodb.net'
```
##### Using IP as Host
Run in terminal
```
source remote-standalone-ip.sh -u '<your_db_username>' -k '<your_db_password>' -h '<your_db_host> -p '<your_db_port>' 
```
Example
```
source remote-standalone-ip.sh -u 'root' -k 'root' -h '127.0.0.1' -p '27017'
```
- #### Automatic
Automate backup process with crontab\
Run in terminal
```
crontab -e
```
And then add your cronjob that suits you the most.
Example of cronjob runs every day at 0:00 AM
```
0 0 * * * source /path/to/scripts/remote-standalone-uri.sh -u '<your_db_username>' -k '<your_db_password>' -h '<your_db_host>'
```
---
### Replica Set

#### Manual

#### Automate



